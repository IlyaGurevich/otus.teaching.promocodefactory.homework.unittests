﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnersControllerBuilder
    {
        private  IRepository<Partner> _partnersRepository;

        public PartnersControllerBuilder() 
        {
        }

        public PartnersControllerBuilder WithRepository(IRepository<Partner> repository)
        {
            _partnersRepository= repository;
            return this;
        }

        public PartnersController Build() => new PartnersController(_partnersRepository);
    }
}
