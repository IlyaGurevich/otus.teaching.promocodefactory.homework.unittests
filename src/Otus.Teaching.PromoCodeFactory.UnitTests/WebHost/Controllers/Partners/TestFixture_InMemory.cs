﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Otus.Teaching.PromoCodeFactory.WebHost;
using System.Configuration;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class TestFixture_InMemory: IDisposable
    {
        public IServiceProvider ServiceProvider { get; set; }

        public IServiceCollection ServiceCollection { get; set; }

        public TestFixture_InMemory() 
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            ServiceCollection = ExtensionServices.GetServiceCollection(configuration, "Tests");
            var serviceProvider = GetServiceProvider();
            ServiceProvider= serviceProvider;
        }

        private IServiceProvider GetServiceProvider() 
        {
            var serviceProvider = ServiceCollection
                .ConfigureInMemoryContext()
                .BuildServiceProvider();
            //SeedProcedureTypes.SeedData(serviceProvider);
            return serviceProvider;

        }

        public void Dispose()
        {
           
        }
    }
}
