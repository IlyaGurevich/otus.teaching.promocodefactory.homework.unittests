﻿using AutoFixture;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class CheckActiveLimitsTests
    {
        private readonly Fixture _fixture = new Fixture();

        private readonly PartnersController _partnersController = new PartnersController(new Mock<IRepository<Partner>>().Object);

        private readonly Partner _partner;

        private readonly PartnerPromoCodeLimit _activeLimit;

        public CheckActiveLimitsTests()
        {
            //Arrange
            _partner = _fixture.Build<Partner>()
                .With(p => p.NumberIssuedPromoCodes, new Random().Next(1, 99))
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>
                {
                    new PartnerPromoCodeLimit
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        CancelDate = null,
                        Limit = 100
                    }
                }).Create();

            _activeLimit = _partner.PartnerLimits.FirstOrDefault(x =>
               !x.CancelDate.HasValue);

            var methodCheckActiveLimit = _partnersController.GetType().GetMethod("CheckActiveLimit", BindingFlags.Instance |
                BindingFlags.Public | BindingFlags.NonPublic);

            //Act
            methodCheckActiveLimit.Invoke(_partnersController, new object[] { _partner, _activeLimit });

        }

        [Fact]
        public void CheckActiveLimit_LimitIsActive_PartnersIssuedPromoCodesEqualZero() 
        {
            //Assert
            _partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        [Fact]
        public void CheckActiveLimit_LimitIsActive_ActiveLimitIsCanceled()
        {
            //Assert
            _activeLimit.CancelDate.HasValue.Should().BeTrue();
        }
    }
}
