﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests: IClassFixture<TestFixture_InMemory>
    {
        private readonly Mock<IRepository<Partner>> _repositoryPartnerMock;

        private readonly IRepository<Partner> _partnerRepository;

        private readonly IRepository<PartnerPromoCodeLimit> _partnerPromoCodeLimitRepository;

        private readonly PartnersController _partnersController;

        private readonly IFixture _autoFixture;

        public SetPartnerPromoCodeLimitAsyncTests(TestFixture_InMemory testFixture) 
        {
            var serviceProvider = testFixture.ServiceProvider;

            _partnerRepository = serviceProvider.GetService<IRepository<Partner>>();
            _partnerPromoCodeLimitRepository = serviceProvider.GetService<IRepository<PartnerPromoCodeLimit>>();

            _autoFixture = new Fixture().Customize(new AutoMoqCustomization());

            _repositoryPartnerMock = _autoFixture.Freeze<Mock<IRepository<Partner>>>();

            _partnersController = _autoFixture.Build<PartnersController>().OmitAutoProperties().Create();
            
            _autoFixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
                .ForEach(b => _autoFixture.Behaviors.Remove(b));
            _autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }

        [Fact] 
        public async void SetPartnerPromoCodeLimit_PartnerIsNotFound_ReturnsNotFound()
        {
            //Arrange
            Guid partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b166");
            Partner partner = null;
            var requestPartnerPromoCodeLimit = _autoFixture.Create<SetPartnerPromoCodeLimitRequest>();
            _repositoryPartnerMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, requestPartnerPromoCodeLimit);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimit_PartnerIsNotActive_ReturnsBadRequestObject()
        {
            //Arrange

            Partner partner = _autoFixture.Build<Partner>().With(p => p.IsActive, false).Create();
            var requestPartnerPromoCodeLimit = _autoFixture.Create<SetPartnerPromoCodeLimitRequest>();
            _repositoryPartnerMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, requestPartnerPromoCodeLimit);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimit_NewPromoCodeLimitIsLessThanOne_ReturnsBadRequestObject()
        {
            //Arrange
            Partner partner = _autoFixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .Create();

            var requestPartnerPromoCodeLimit = _autoFixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(r => r.Limit, -1)
                .Create();

            _repositoryPartnerMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, requestPartnerPromoCodeLimit);

            //Assert

            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimit_DatabaseSaveNewLimit_DatabaseHasNewObject() 
        {
            //Arrange
            Partner partner = _autoFixture.Build<Partner>()
               .With(p => p.IsActive, true)
               .Create();

            await _partnerRepository.AddAsync(partner);

            var requestPartnerPromoCodeLimit = _autoFixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(p => p.EndDate, DateTime.Now.AddDays(10))
                .With(p => p.Limit, 100)
                .Create();

            PartnersController partnersController = new PartnersControllerBuilder()
                .WithRepository(_partnerRepository)
                .Build();

            //Act
            CreatedAtActionResult result = (CreatedAtActionResult)partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, requestPartnerPromoCodeLimit).Result;

            Dictionary<string,Guid> ids = (Dictionary<string, Guid>)result.Value;

            Guid limitId = ids["limitId"];

            PartnerPromoCodeLimit limit = _partnerPromoCodeLimitRepository.GetByIdAsync(limitId).Result;

            //Assert
            limit.Should().NotBeNull();

        }
    }
}